(function($){

Drupal.behaviors.poseidon = function(context) {
	$('#search-sites').keyup(function() {
		if ($(this).val() == '') {
			$('#clear-search').fadeOut();	
		}	
		else {
			$('#clear-search').fadeIn();	
		} 
		
		$('.site-item.contains').removeClass('contains');		
		$('.site-item:contains("' + $('#search-sites').val() + '")').addClass('contains');
		$('.site-item:not(".contains")').hide();
		$('.site-item.contains').fadeIn();		
		
		if ($('.hide-platforms.hidden').length) {	
			$.each($('.platform-header'), function(index, value) {
				if($(this).nextUntil('.platform-header', '.site-item:visible').size() > 0) { 
					$(this).fadeIn();
				} 
				else {
					$(this).fadeOut();
				}
			});
			
		}
	});		
	
	
	if (context == document) {
		if ($('#search-sites').val() == '') {
			$('#clear-search').hide();	
		}		
		switchForm('show');
		$.each($('.poseidon-site-thumb'), function(index, value) { 
			$.get(Drupal.settings.basePath + 'poseidon/refresh-thumb/' + $(this).attr('data'), function(data) {
  			console.log(data);
			});
		});
	}	
	
	$('.site-item .form-option input').click(function() {
		switchForm('fade');
	});

	
	$('#clear-search').click(function() {
		$('#search-sites').val('').keyup();
	});

	var state = Drupal.poseidon.getState('poseidonState');
	if (state == false) {
		$('#right.sidebar:not(".processed")').addClass('processed').before('<span id="sidebar-collapser" class="collapse">>></span>');
	}
	else {
		$('#right.sidebar:not(".processed")').addClass('processed').before('<span id="sidebar-collapser" class="expand"><<</span>');
		$('body').addClass('sidebar-collapsed');		
	}
	

	var statePlatforms = Drupal.poseidon.getState('poseidonStatePlatforms');

	if (statePlatforms == false) {	
		$('.platform-header').hide();
		$('.hide-platforms').removeClass('hidden').html('<strong>+</strong> ' + Drupal.t('Show platforms'));
	} 
	
	$('.hide-platforms').click(function() {
		togglePlatforms();
		return false;
	});	
	
	$('#sidebar-collapser').click(function() {
		if ($(this).hasClass('collapse') == true) {
			$('#right.sidebar').fadeOut('slow', function() {
				$('body').addClass('sidebar-collapsed');
			});
			$(this).addClass('expand').removeClass('collapse').text('<<');	
			Drupal.poseidon.setState('poseidonState', true);
		}
		else {
			$('body').removeClass('sidebar-collapsed');
			$('#right.sidebar').fadeIn('slow', function() {
			});
			$(this).addClass('collapse').removeClass('expand').text('>>');		
			Drupal.poseidon.setState('poseidonState', 0);
		}
	});

	function togglePlatforms() {
		if ($('.hide-platforms').hasClass('hidden')) {
			Drupal.poseidon.setState('poseidonStatePlatforms', 0);
			$('.platform-header').slideUp();

			$('.hide-platforms').removeClass('hidden').html('<strong>+</strong> ' + Drupal.t('Show platforms'));
		}	
		else {
			Drupal.poseidon.setState('poseidonStatePlatforms', true);						
			$('.platform-header').slideDown();
			$('.hide-platforms').addClass('hidden').html('<strong>-</strong> ' + Drupal.t('Hide platforms'));
			$.each($('.platform-header'), function(index, value) {
				if($(this).nextUntil('.platform-header', '.site-item:visible').size() > 0) { 
					$(this).fadeIn();
				} 
				else {
					$(this).hide();
				}
			});				
		}	
	}

}

function switchForm(method) {
	if($('.site-item .form-option input:checked').length > 0) {
		if (method == 'show') {
			$('#hosting-site-list-form .container-inline').show();
		} else if (method == 'fade') {
			$('#hosting-site-list-form .container-inline').slideDown();
		}
	}
	else {
		if (method == 'show') {		
			$('#hosting-site-list-form .container-inline').hide();
		} else if (method == 'fade') {
			$('#hosting-site-list-form .container-inline').slideUp();
		}
	}
}
})(jq171)

Drupal.poseidon = {};

/**
 * Get the value of a cookie variable.
 */
Drupal.poseidon.getState = function(key) {
  if (!Drupal.poseidon.state) {
    Drupal.poseidon.state = {};
    var cookie = $.cookie('poseidon');
    var query = cookie ? cookie.split('&') : [];
    if (query) {
      for (var i in query) {
        // Extra check to avoid js errors in Chrome, IE and Safari when
        // combined with JS like twitter's widget.js.
        // See http://drupal.org/node/798764.
        if (typeof(query[i]) == 'string' && query[i].indexOf('=') != -1) {
          var values = query[i].split('=');
          if (values.length === 2) {
            Drupal.poseidon.state[values[0]] = values[1];
          }
        }
      }
    }
  }
  return Drupal.poseidon.state[key] ? Drupal.poseidon.state[key] : false;
};

/**
 * Set the value of a cookie variable.
 */
Drupal.poseidon.setState = function(key, value) {
  var existing = Drupal.poseidon.getState(key);
  if (existing != value) {
    Drupal.poseidon.state[key] = value;
    var query = [];
    for (var i in Drupal.poseidon.state) {
      query.push(i + '=' + Drupal.poseidon.state[i]);
    }
    $.cookie('poseidon', query.join('&'), {expires: 7, path: '/'});
  }
};